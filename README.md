```
                   ::::::::          
         :+:      :+:    :+:         
    +++++++++++  +:+         +++++   
       +:+      +#+         +#  +#   
      +#+      +#+         +#        
     #+#      #+#     +#  +#  +#     
    ###       ########+   ####+      

 tCc|MC_Crafty
 mc_crafty@gmx.com

```

# Maven Dependency Tree to HTML jsTree converter

## deptree2html

```
Usage: deptree2html [options]

Description: convert mvn dependency tree to html

This application does not expect any arguments

Options:
  -f, --file_name <string>  The input file containing the output of mvn dependency:tree . Default: examples/example.txt
  -o, --output_directory <string>
                            The output directory where the HTML/JS will be saved. Default: .
  -a, --artifact <string>   The top level artifact we generated the tree for. No Default
  -l, --log_level <string>  The logging level: [ERROR, WARN, INFO, DEBUG] . Default: INFO
  -h, --help                display this help and exit
  --version                 output version information and exit
```

The initial version was using the `templates/page/template.html` to create a self contained tree style based on [W3 Tree View](https://www.w3schools.com/howto/howto_js_treeview.asp). That has been deprecated.

This is currently using [jsTree](https://www.jstree.com/) and based on the [scala dependencyTreeBrowse plugin](https://github.com/sbt/sbt-dependency-graph/tree/master)

