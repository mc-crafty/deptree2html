println('Removing old artifact...')
rm('bin/deptree2html') or {}
println('Done removing old artifact.')

println('Installing dependencies...')
execute_or_panic('${quoted_path(@VEXE)} install')
println('Finished fetching and installing dependencies. ')

println('Compiling and building executable...')
execute_or_panic('${quoted_path(@VEXE)} . -prod -o bin/deptree2html')
println('Done compiling and placing executable in "bin".')
