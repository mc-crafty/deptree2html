set -x
rm ./bin/deptree2html
v install \
        && v fmt -verify . \
	&& v test . \
	&& v . -prod -o ./bin/deptree2html
