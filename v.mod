Module {
	name: 'deptree2html'
	description: 'convert mvn dependency tree to html'
	version: '2.3.2'
	license: 'MIT'
	dependencies: ['tobealive.bartender']
}
