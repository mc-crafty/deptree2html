module main

import os
import log
import json
import flag
import math
import regex
import tobealive.bartender

// vfmt off
const header       = r'---\s.*maven-dependency-plugin'
const indent       = '|  '
const indent_space = '   '
const entry        = '+- '
const inline_entry = '     +- '
const last         = '\\- '
const inline_last  = '     \\- '
const footer       = '-----'
// vfmt on

struct Settings {
	ifname    string
	ofname    string
	artifact  string
	log_level string
}

fn main() {
	settings := args()
	mut logger := get_logger(settings)
	input_file := read_input_file(settings, mut logger)
	dep_to_html(input_file, settings, mut logger)
}

// read input file from supplied ifname arg or stdin
fn read_input_file(settings Settings, mut logger log.Log) []string {
	mut input_file := []string{}
	if settings.ifname.len == 0 {
		logger.info('No input file specified, attempting to read from stdin...')
		mut line := ''
		for {
			line = os.get_raw_line()
			logger.debug('line: ${line}')
			if line.len <= 0 {
				break
			}
			input_file << line
		}
	} else {
		logger.info('Reading input file...')
		input_file = os.read_lines(settings.ifname) or { panic(err) }
	}

	if input_file.len == 0 {
		eprintln('Could not read input')
		exit(1)
	}

	return input_file
}

// Read input flags and options
fn args() Settings {
	mut fp := flag.new_flag_parser(os.args)
	ifname := fp.string('file_name', `f`, '', 'The input file containing the output of mvn dependency:tree . If not supplied, the program will read from stdin, such as piped output. No Default.')
	ofname := fp.string('output_directory', `o`, '.', 'The output directory where the HTML/JS will be saved. Default: .')
	artifact := fp.string('artifact', `a`, '', 'The top level artifact we generated the tree for. No Default')
	log_level := fp.string('log_level', `l`, 'INFO', 'The logging level: [ERROR, WARN, INFO, DEBUG] . Default: INFO').to_upper()
	fp.limit_free_args(0, 0) or { panic(err) }
	fp.application(name)
	fp.version(version)
	fp.description(description)
	fp.skip_executable()
	fp.finalize() or { panic(err) }

	return Settings{ifname, ofname, artifact, log_level}
}

// Return a logger matching the level specified in settings object
fn get_logger(settings Settings) log.Log {
	mut l := log.Log{}
	l.set_level(log.level_from_tag(settings.log_level) or { panic(err) })
	return l
}

// Convert dependency tree to html tree
fn dep_to_html(deps []string, settings Settings, mut logger log.Log) {
	logger.debug('dep_to_html')
	mut b := bartender.Bar{
		width: 60
		pre: '|'
		post: bartender.Affix{
			pending: '| Converting...'
			finished: '| Done!'
		}
	}
	start_index := find_start(deps, settings.artifact, mut logger) or { panic(err) }
	b.progress() or { panic(err) }
	logger.debug('tree starts at ${start_index}')
	mut ul_root := parse_initial_artifact(settings.artifact, deps[start_index], mut logger)
	b.progress() or { panic(err) }
	ul_root = parse_deps(deps[start_index + 1..], ul_root, mut logger, mut b)
	logger.debug(ul_root.str())
	logger.info('Writing output files...')
	embedded_tree := $embed_file('resources/tree.html')
	os.write_file('${settings.ofname}/tree.html', embedded_tree.to_string()) or { panic(err) }
	os.write_file('${settings.ofname}/tree.data.js', 'tree_data = ${json.encode(ul_root)};') or {
		panic(err)
	}
	logger.info('Done!')
}

// Find the index which starts the dependency tree
fn find_start(deps []string, artifact string, mut logger log.Log) !int {
	header_index := find_header(deps)!
	logger.debug('found header at ${header_index}')
	for i, dep in deps[header_index..] {
		dep.index(artifact) or { continue }
		logger.debug('found artifact at ${i}')
		return header_index + i
	}
	return error('maven dependency tree does not contain initial artifact ${artifact}')
}

// Find the header where Maven starts outputting the dependency tree
fn find_header(deps []string) !int {
	for i, dep in deps {
		mut re := regex.regex_opt(header) or { panic(err) }
		start, _ := re.find(dep)
		if start == -1 {
			continue
		}
		return i
	}
	return error('maven dependency tree header not found')
}

// Parse the initial root
fn parse_initial_artifact(artifact string, dep string, mut logger log.Log) ULTreeNode {
	index := dep.index(artifact) or { panic(err) }
	logger.debug(dep[index..])
	return ULTreeNode{
		text: dep[index..]
	}
}

// Returns the root with updated children
fn parse_deps(deps []string, root ULTreeNode, mut logger log.Log, mut b bartender.Bar) ULTreeNode {
	mut root_stack := [root]
	mut prev_il := -1

	// Handle progress
	mut p_remaining := b.iters - 2
	mut p_step := 1
	mut p_makeup := 1
	logger.debug('p_remaining = ${p_remaining}, deps = ${deps.len}')
	// More lines than progress indicators, update indicators at a pace matching line progress
	if deps.len > p_remaining {
		p_step = int(math.round(f64(deps.len) / f64(p_remaining)))
		logger.debug('p_step = ${p_step} = round(${f64(deps.len) / f64(p_remaining)})')
	} else {
		// More progress indicators than lines, update progress multiple times per line
		p_makeup = int(math.round(p_remaining / deps.len))
		logger.debug('p_makeup = ${p_makeup} = round(${p_remaining / deps.len})')
	}

	for i, dep in deps {
		logger.debug(dep)

		// Fill the progress bar
		if p_makeup > 1 || (p_remaining > 0 && i % p_step == 0 && i != 0) {
			for _ in 0 .. p_makeup {
				p_remaining--
				b.progress() or { panic(err) }
			}
		}

		// Footer means no more dependencies to check
		if dep.contains(footer) {
			logger.debug('found footer')
			break
		}

		// Handle indent level change -- change in indent is change in parent
		mut il_space := dep.count(indent_space)
		mut il_marker := dep.count(indent)
		logger.debug('root_stack.len = ${root_stack.len}, il_marker = ${il_marker}, il_space = ${il_space}, prev_il = ${prev_il}')
		if il_marker == -1 {
			il_marker = 0
		}
		if il_space == -1 {
			il_space = 0
		}
		mut il := il_marker + il_space

		// Maven outputs inline dependencies without additional indentation markers
		// TODO : cleanup these edge cases -- possibly consolidate with il<= prev_il
		if dep.contains(inline_last) {
			logger.debug('inline last ${dep.after(inline_last)}')
			logger.debug('adding as child of previous last ${root_stack.last().children.last().text}')
			node := ULTreeNode{
				text: dep.after(inline_last)
			}
			mut c := root_stack.last().children.clone()
			mut update := c.pop()
			update.children << node
			c << update
			root_stack.last().children = c
			continue
		}
		if dep.contains(inline_entry) {
			logger.debug('inline entry ${dep.after(inline_entry)}')
			logger.debug('adding as child of ${root_stack.last().text}')
			node := ULTreeNode{
				text: dep.after(inline_entry)
			}
			mut c := root_stack.last().children.clone()
			mut update := c.pop()
			update.children << node
			c << update
			root_stack.last().children = c
			continue
		}

		// Indent moved back -- current parent is finished
		// TODO : play with using & op to modify in place
		if il <= prev_il {
			update := root_stack.pop() // grab update children
			// remove previous child added
			mut c := root_stack.last().children.clone()
			c.pop()
			root_stack.last().children = c
			// replace with current child having updated dependencies
			root_stack.last().children << update
		}
		if il != prev_il {
			prev_il = il
		}

		// Entry has the possibility to be a parent itself
		if dep.contains(entry) {
			logger.debug('entry ${dep.after(entry)}')
			logger.debug('adding as child of ${root_stack.last().text}')
			node := ULTreeNode{
				text: dep.after(entry)
			}
			root_stack.last().children << node
			root_stack << node
			continue
		}

		// Last dependency for this parent
		if dep.contains(last) {
			logger.debug('last ${dep.after(last)}')
			logger.debug('adding as child of ${root_stack.last().text}')
			node := ULTreeNode{
				text: dep.after(last)
			}
			root_stack.last().children << node
			continue
		}

		logger.debug('\n')
	}

	// Fill any additional progress indicators left over
	for _ in 0 .. p_remaining {
		b.progress() or { panic(err) }
	}

	logger.debug('${root_stack.len}')

	return root_stack.first()
}
