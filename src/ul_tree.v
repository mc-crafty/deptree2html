module main

import strings

// vfmt off
const ul_replace      = '<ul id="treeUL"></ul>'
const ul_header       = '${ul_replace}\n'
const ul_fanout_start = '<li><span class="caret">'
const ul_fanout       = '</span>\n<ul class="nested">\n'
const ul_fanout_end   = '</ul>\n</li>\n'
const ul_entry_start  = '<li>'
const ul_entry_end    = '</li>\n'
const ul_footer       = '</ul>\n'
// vfmt on

// Struct to represent the Unordered List implementation of a Maven Dependency Tree
struct ULTreeNode {
pub mut:
	text     string
	children []ULTreeNode
}

fn (root ULTreeNode) update_template(template string) string {
	return template.replace(ul_replace, root.str())
}

// Generate the HTML from this node as top level dependency
fn (root ULTreeNode) str() string {
	mut sb := strings.new_builder(200)
	sb.write_string(ul_header)
	node_str(root, mut sb)
	sb.write_string(ul_footer)
	return sb.str()
}

// Recursively generate the HTML for all dependencies
fn node_str(root ULTreeNode, mut sb strings.Builder) {
	is_fanout := root.children.len > 0
	if is_fanout {
		sb.write_string(ul_fanout_start)
		sb.write_string(root.text)
		sb.write_string(ul_fanout)
		for node in root.children {
			node_str(node, mut sb)
		}
		sb.write_string(ul_fanout_end)
	} else {
		sb.write_string(ul_entry_start)
		sb.write_string(root.text)
		sb.write_string(ul_entry_end)
	}
}
