module main

import os
import json
import log
import tobealive.bartender

fn test_find_start() {
	settings, mut logger, _ := before()
	deps := os.read_lines(settings.ifname) or { panic(err) }

	result := find_start(deps, settings.artifact, mut logger) or { panic(err) }

	assert result == 644
}

fn test_find_header() {
	settings, mut logger, _ := before()
	deps := os.read_lines(settings.ifname) or { panic(err) }

	result := find_header(deps)!

	assert 334 == result
}

fn test_parse_initial_artifact() {
	settings, mut logger, _ := before()
	deps := os.read_lines(settings.ifname) or { panic(err) }

	result := parse_initial_artifact(settings.artifact, deps[644], mut logger)

	assert result.text == 'io.vertx:vertx-core:jar:5.0.0-SNAPSHOT'
}

fn test_parse_deps() {
	settings, mut logger, mut b := before()
	deps := os.read_lines(settings.ifname) or { panic(err) }
	mut initial_artifact := parse_initial_artifact(settings.artifact, deps[644], mut logger)

	b.progress() or { panic(err) }
	b.progress() or { panic(err) }
	result := parse_deps(deps[645..], initial_artifact, mut logger, mut b)

	expected_json := os.read_file('examples/example-tree.data.js') or { panic(err) }
	result_json := 'tree_data = ${json.encode(result)};'
	assert result_json == expected_json
}

fn test_dep_to_html() {
	settings, mut logger, mut b := before()
	deps := os.read_lines(settings.ifname) or { panic(err) }

	dep_to_html(deps, settings, mut logger)

	expected_json := os.read_file('examples/example-tree.data.js') or { panic(err) }
	result_json := os.read_file('${settings.ofname}/tree.data.js') or { panic(err) }

	os.rm('${settings.ofname}/tree.data.js') or { panic(err) }
	os.rm('${settings.ofname}/tree.html') or { panic(err) }

	assert result_json == expected_json
}

fn before() (Settings, log.Log, bartender.Bar) {
	settings := get_settings()
	return settings, get_logger(settings), get_progress_bar()
}

fn get_settings() Settings {
	return Settings{'examples/example.txt', 'examples', 'io.vertx:vertx-core', 'INFO'}
}

fn get_progress_bar() bartender.Bar {
	return bartender.Bar{
		width: 60
		pre: '|'
		post: bartender.Affix{
			pending: '| Converting...'
			finished: '| Done!'
		}
	}
}
