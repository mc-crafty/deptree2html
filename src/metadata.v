module main

import v.vmod

const embedded_vmod = $embed_file('v.mod')

const manifest = vmod.decode(embedded_vmod.to_string()) or { panic(err) }

pub const version = manifest.version

pub const name = manifest.name

pub const description = manifest.description
